<?php

function apexchat_admin_settings_form($form_state) {

	$form['account'] = array(
		'#type' => 'fieldset',
		'#title' => t('General settings'),
	);

	$form['account']['apexchat_accountcode'] = array(
		'#title' => t('Company Shortcode'),
		'#type' => 'textfield',
		'#default_value' => variable_get('apexchat_accountcode', ''),
		'#size' => 15,
		'#required' => TRUE,
		'#description' => t('This ID is unique to each site.'),
	);


	/**
	 * Chat scope options
	 */
	$form['scope_title'] = array(
    '#type' => 'item',
    '#title' => t('Chat scope'),
	);

	$form['scope'] = array(
		'#type' => 'vertical_tabs'
	);


	/**
	 * Page visibility settings
	 */

  // Page specific visibility configurations.
  $php_access = user_access('use PHP for tracking visibility');
  $visibility = variable_get('apexchat_visibility_pages', 0);
  $pages = variable_get('apexchat_pages', APEXCHAT_PAGES);

  $form['scope']['page_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pages'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  if ($visibility == 2 && !$php_access) {
    $form['scope']['page_vis_settings'] = array();
    $form['scope']['page_vis_settings']['apexchat_visibility_pages'] = array('#type' => 'value', '#value' => 2);
    $form['scope']['page_vis_settings']['apexchat_pages'] = array('#type' => 'value', '#value' => $pages);
  }
  else {
    $options = array(
      t('Every page except the listed pages'),
      t('The listed pages only'),
    );
    $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));

    if (module_exists('php') && $php_access) {
      $options[] = t('Pages on which this PHP code returns <code>TRUE</code> (experts only)');
      $title = t('Pages or PHP code');
      $description .= ' ' . t('If the PHP option is chosen, enter PHP code between %php. Note that executing incorrect PHP code can break your Drupal site.', array('%php' => '<?php ?>'));
    }
    else {
      $title = t('Pages');
    }
    $form['scope']['page_vis_settings']['apexchat_visibility_pages'] = array(
      '#type' => 'radios',
      '#title' => t('Add chat to specific pages'),
      '#options' => $options,
      '#default_value' => $visibility,
    );
    $form['scope']['page_vis_settings']['apexchat_pages'] = array(
      '#type' => 'textarea',
      '#title' => $title,
      '#title_display' => 'invisible',
      '#default_value' => $pages,
      '#description' => $description,
      '#rows' => 10,
    );
  }


	/**
	 * Role visibility settings
	 */
	
	$form['scope']['role_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Roles'),
	);

  $form['scope']['role_vis_settings']['apexchat_visibility_roles'] = array(
    '#type' => 'radios',
    '#title' => t('Add scope for specific roles'),
    '#options' => array(
      t('Add to the selected roles only'),
      t('Add to every role except the selected ones'),
    ),
    '#default_value' => variable_get('apexchat_visibility_roles', 0),
  );

  $role_options = array_map('check_plain', user_roles());
  $form['scope']['role_vis_settings']['apexchat_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Roles'),
    '#default_value' => variable_get('apexchat_roles', array()),
    '#options' => $role_options,
    '#description' => t('If none of the roles are selected, all users will be tracked. If a user has any of the roles checked, that user will be tracked (or excluded, depending on the setting above).'),
  );



	return system_settings_form($form);
}


/**
 * Implements _form_validate()
 */
function apexchat_admin_settings_form_validate($form, &$form_state) {

	$form_state['values']['apexchat_accountcode'] = trim($form_state['values']['apexchat_accountcode']);
	$form_state['values']['apexchat_pages'] = trim($form_state['values']['apexchat_pages']);
}